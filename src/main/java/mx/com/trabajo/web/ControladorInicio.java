package mx.com.trabajo.web;

import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import mx.com.trabajo.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import mx.com.trabajo.domain.Usuario;
import org.springframework.validation.Errors;

/**
 *
 * @author Gabriel Moreno <gabriel@newlandapps.com>
 */
@Controller
@Slf4j
public class ControladorInicio {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/")
    public String inicio(Model model) {
        var usuarios = usuarioService.listarUsuarios();
        log.info("ejecutando el controlador Spring MVC");
        model.addAttribute("usuarios", usuarios);
        return "index.xhtml";
    }

    @GetMapping("/agregar")
    public String agregar(Usuario user) {
        return "modificar.xhtml";
    }

    @PostMapping("/guardar")
    public String guardar(@Valid Usuario user, Errors errores) {
        if(errores.hasErrors()){
            return "modificar.xhtml";
        }
        usuarioService.guardar(user);
        return "redirect:/";
    }

    @GetMapping("/modificar/{idUsuario}")
    public String editar(Usuario user, Model model) {
        user = usuarioService.encontrarUsuario(user);
        model.addAttribute("usuario",user);
        return "modificar.xhtml";
    }
    
    @GetMapping("/eliminar")
    public String eliminar(Usuario user){
        usuarioService.eliminar(user);
        return "redirect:/";
    }

}
