package mx.com.trabajo.dao;

import org.springframework.data.repository.CrudRepository;
import mx.com.trabajo.domain.Usuario;

/**
 *
 * @author Gabriel Moreno <gabriel@newlandapps.com>
 */
public interface UsuarioDAO extends CrudRepository<Usuario, Long>{
    
}
