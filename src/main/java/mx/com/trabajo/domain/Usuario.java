package mx.com.trabajo.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Gabriel Moreno <gabriel@newlandapps.com>
 */
@Data
@Entity
@Table(name = "cat_usuarios")
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idUsuario;
    
    @NotEmpty
    private String cUsuario;
    
    private int bStatus;
    
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dCreation;
}
