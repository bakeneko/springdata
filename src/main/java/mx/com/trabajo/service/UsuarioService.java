package mx.com.trabajo.service;

import java.util.List;
import mx.com.trabajo.domain.Usuario;

/**
 *
 * @author Gabriel Moreno <gabriel@newlandapps.com>
 */
public interface UsuarioService {

    public List<Usuario> listarUsuarios();

    public void guardar(Usuario user);

    public void eliminar(Usuario user);

    public Usuario encontrarUsuario(Usuario user);

}
