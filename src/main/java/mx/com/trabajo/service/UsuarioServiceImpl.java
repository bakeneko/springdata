package mx.com.trabajo.service;

import java.util.List;
import mx.com.trabajo.dao.UsuarioDAO;
import mx.com.trabajo.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gabriel Moreno <gabriel@newlandapps.com>
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Override
    @Transactional(readOnly = true)
    public List<Usuario> listarUsuarios() {
        return (List<Usuario>) usuarioDAO.findAll();
    }

    @Override
    @Transactional
    public void guardar(Usuario user) {
        usuarioDAO.save(user);
    }

    @Override
    @Transactional
    public void eliminar(Usuario user) {
        usuarioDAO.delete(user);
    }

    @Override
    @Transactional(readOnly = true)
    public Usuario encontrarUsuario(Usuario user) {
        return usuarioDAO.findById(user.getIdUsuario()).orElse(null);
    }

}
