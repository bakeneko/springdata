package mx.com.trabajo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolaSpringDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolaSpringDataApplication.class, args);
	}

}
